@extends('layouts.home')
@section('title', '| Home')
{{-- Dashboard
@endsection --}}

@section('page_title')
    <p>Dashboard</p>
@endsection

@section('container')
    @include('customers.products.detail')
    @include('partials.home.right-product-detail')
@endsection
