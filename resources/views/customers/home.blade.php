@extends('layouts.home')
@section('title', '| Home')
{{-- Dashboard
@endsection --}}

@section('page_title')
    <p>Dashboard</p>
@endsection

@section('container')
    @include('partials.home.banner')

    <div class="clearfix"></div>
    <div id="content_center">
        <div class="top_area_list_page">
            <h1>Máy tính xách tay</h1>
            <div class="sort_style">
                <span>Lựa chọn</span>
                <a onclick="setUserOption('product_display', 'list', 'https://laptopworld.vn/may-tinh-xach-tay.html')" href="javascript:void(0)"><i class="fa fa-list"></i></a>
                <a class="active" onclick="setUserOption('product_display', 'grid', 'https://laptopworld.vn/may-tinh-xach-tay.html')" href="javascript:void(0)"><i class="fa fa-th-large"></i></a>
            </div>
            <div class="sort_pro">
                <select onchange="location.href=this.value">
                    <option value="">Sắp xếp sản phẩm</option>
                    <option value="https://laptopworld.vn/may-tinh-xach-tay.html?sort=new">Mới nhất</option>
                    <option value="https://laptopworld.vn/may-tinh-xach-tay.html?sort=price-asc">Giá tăng dần</option>
                    <option value="https://laptopworld.vn/may-tinh-xach-tay.html?sort=price-desc">Giá giảm dần</option>
                    <option value="https://laptopworld.vn/may-tinh-xach-tay.html?sort=view">Lượt xem</option>
                </select>
            </div>

            <div class="paging">

                <a href="#" class="current">1</a>

                <a href="/may-tinh-xach-tay.html?page=2">2</a>

                <a href="/may-tinh-xach-tay.html?page=3">3</a>

                <a href="/may-tinh-xach-tay.html?page=4">4</a>

                <a href="/may-tinh-xach-tay.html?page=5">5</a>

                <a href="/may-tinh-xach-tay.html?page=6">6</a>

                <a href="/may-tinh-xach-tay.html?page=7">7</a>

                <a href="/may-tinh-xach-tay.html?page=2">Next</a>

            </div><!--paging-->

        </div><!--top_area_list_page-->

        <div class="product-list">

            <div class="p-item">
                <div class="p-container">
                    <a href="/msi-modern-14-a10m-692vn.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5177_msi_modern_14_a10m_1.jpg" data-src="/media/product/250_5177_msi_modern_14_a10m_1.jpg" alt="MSI Modern 14 A10M-692VN" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">17.990.000</span>
                        <!-- <span class="price-shadow">17.990.000</span> -->
    </span>

                    <span class="p-old-price"></span>

                    <a href="/msi-modern-14-a10m-692vn.html" class="p-name">MSI Modern 14 A10M-692VN</a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(5177,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/msi-modern-14-a10m-692vn.html" class="hover_name">MSI Modern 14 A10M-692VN</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">17.990.000</span>
              <!-- <span class="price-shadow">17.990.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>24 Tháng chính hãng MSI Việt Nam</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        NPP: VS
                        <br>

                        CPU: Intel Core i5-10210U (1.6GHz up to 4.2GHz 6MB Cache)
                        <br>

                        Ram: 8GB DDR4 2666MHz
                        <br>

                        Ổ cứng: 256GB PCIe NVMe™ M.2 SSD
                        <br>

                        VGA: Intel UHD Graphics
                        <br>

                        Display: 14" FHD (1920 x 1080) IPS-Level, 60Hz, 72% NTSC, Thin Bezel, 100% sRGB
                        <br>

                        Pin: 4 Cell 50WHr
                        <br>

                        Weight: 1.19 kg
                        <br>

                        Color: Silver
                        <br>

                        OS: Windows 10 Home bản quyền<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        - Túi bọc Sleeve Bag
                        - Chuột không dây Xtech 7073
                        - Bàn di chuột<br>

                        "Check-in" Facebook Nhận ngay Tai nghe Wangming 9600 trị giá 290.000Đ (Từ 01/05 - 31/05/2020)<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/lg-gram-2020-14zd90n-v.ax53a5.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5755_14zd90n_v_ax53a5.jpg" data-src="/media/product/250_5755_14zd90n_v_ax53a5.jpg" alt="LG Gram 2020 14ZD90N-V.AX53A5" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">28.990.000</span>
                        <!-- <span class="price-shadow">28.990.000</span> -->
    </span>

                    <span class="p-old-price"></span>

                    <a href="/lg-gram-2020-14zd90n-v.ax53a5.html" class="p-name">LG Gram 2020 14ZD90N-V.AX53A5</a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(5755,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/lg-gram-2020-14zd90n-v.ax53a5.html" class="hover_name">LG Gram 2020 14ZD90N-V.AX53A5</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">28.990.000</span>
              <!-- <span class="price-shadow">28.990.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng chính hãng LG Việt Nam, bảo hành tại nhà</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel Core i5-1035G7 (1.2GHz up to 3.7GHz 6MB Cache)
                        <br>

                        Ram: 8GB DDR4 2400MHz Onboard (Còn 1 khe ram, up to 24GB SDRAM)
                        <br>

                        Ổ cứng: 256GB PCIe® NVMe™ M.2 SSD (còn trống 1 khe SSD)
                        <br>

                        Display: 14" FHD (1920 x 1080) IPS, sRGB 96%, 300nits
                        <br>

                        VGA: Intel® Iris® Plus Graphics
                        <br>

                        Pin: 4 Cell 72WHr (9,450 mAh)
                        <br>

                        Color: White
                        <br>

                        Weight: 999 g
                        <br>

                        OS: Free DOS<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        Hoàn tiền 1.500.000Đ (Chưa trừ vào giá)
                        Set quà trị giá 6.000.000Đ (Từ 01/05 - 31/05/2020) bao gồm:
                        - 1 túi chống shock thời trang LG
                        - 1 chuột pebble Logitech
                        - 1 ổ cứng WD SSD 512 GB<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/msi-prestige-15-a10sc-222vn.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5176_" data-src="/media/product/250_5176_" alt="MSI Prestige 15 A10SC 222VN" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">31.790.000</span>
                        <!-- <span class="price-shadow">31.790.000</span> -->
    </span>


                    <span class="p-old-price">33.990.000 VND</span>
                    <span class="p-discount">-7%</span>

                    <a href="/msi-prestige-15-a10sc-222vn.html" class="p-name">MSI Prestige 15 A10SC 222VN</a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(5176,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/msi-prestige-15-a10sc-222vn.html" class="hover_name">MSI Prestige 15 A10SC 222VN</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">31.790.000</span>
              <!-- <span class="price-shadow">31.790.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
            <span class="marketPrice">
                <i>33.990.000</i>
            </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>24 Tháng chính hãng MSI Việt Nam</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        NPP: VX
                        <br>

                        CPU: Intel Core i7-10710U (1.1GHz up to 4.7GHz, 12MB Cache)
                        <br>

                        Ram: 16GB (8GB x2) DDR4 2666MHz
                        <br>

                        Ổ cứng: 512GB PCIe NVMe™ M.2 SSD
                        <br>

                        VGA: NVIDIA GeForce GTX 1650 4GB GDDR5 with Max-Q Design
                        <br>

                        Dispaly: 15.6" FHD (1920 x 1080) IPS, 60Hz, Thin Bezel, Anti-Glare with 72% NTSC, 100% sRGB
                        <br>

                        Color: Black
                        <br>

                        Pin: 4 Cell 82WHr
                        <br>

                        Weight: 1.6 kg
                        <br>

                        OS: Windows 10 Home<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        - Túi bọc Sleeve Bag
                        - Chuột không dây Xtech 7073
                        - Bàn di chuột.<br>

                        "Check-in" Facebook Nhận ngay Tai nghe Wangming 9600 trị giá 290.000Đ (Từ 01/05 - 31/05/2020)<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/dell-latitude-7490-core-i7-8650u-ram-16gb-512gb-ssd.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_395_dell_7490.jpg" data-src="/media/product/250_395_dell_7490.jpg" alt="Dell Latitude 7490 (Core i7-8650U / Ram 16GB / 512GB SSD)" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">27.990.000</span>
                        <!-- <span class="price-shadow">27.990.000</span> -->
    </span>


                    <span class="p-old-price">28.990.000 VND</span>
                    <span class="p-discount">-4%</span>

                    <a href="/dell-latitude-7490-core-i7-8650u-ram-16gb-512gb-ssd.html" class="p-name">Dell Latitude 7490 (Core i7-8650U / Ram 16GB / 512GB SSD)</a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(395,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/dell-latitude-7490-core-i7-8650u-ram-16gb-512gb-ssd.html" class="hover_name">Dell Latitude 7490 (Core i7-8650U / Ram 16GB / 512GB SSD)</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">27.990.000</span>
              <!-- <span class="price-shadow">27.990.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
            <span class="marketPrice">
                <i>28.990.000</i>
            </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 tháng tại công ty Hoàng Minh Tech Co</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: 8th Gen Intel® Core™ i7-8650U Processor (Quad Core, 8M Cache, 1.9GHz up to 4.2GHz,15W, vPro)
                        <br>

                        RAM: 16GB DDR4 2400MHz
                        <br>

                        Ổ cứng: M.2 PCIe NVMe Solid State Drive (M.2 SSD) 512GB
                        <br>

                        Display: 14 inch FHD (1920 x 1080)  LED Backlight
                        <br>

                        VGA: Share Intel® UHD Graphics 620
                        <br>

                        1 x USB-C/DisplayPort , 3 x USB 3.1
                        <br>

                        Pin: 4cell 65Wh
                        <br>

                        Weight: 1.4kg
                        <br>

                        OS: Windows 10 Pro Bản Quyền.<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        Túi xách Dell + Chuột không dây cao cấp Xtech TM-G68 + Bàn di chuột.<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/lg-gram-2020-14zd90n-v.ax55a5.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5757_v_ax55a5.jpg" data-src="/media/product/250_5757_v_ax55a5.jpg" alt="LG Gram 2020 14ZD90N-V.AX55A5" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">30.490.000</span>
                        <!-- <span class="price-shadow">30.490.000</span> -->
    </span>

                    <span class="p-old-price"></span>

                    <a href="/lg-gram-2020-14zd90n-v.ax55a5.html" class="p-name">LG Gram 2020 14ZD90N-V.AX55A5</a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(5757,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/lg-gram-2020-14zd90n-v.ax55a5.html" class="hover_name">LG Gram 2020 14ZD90N-V.AX55A5</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">30.490.000</span>
              <!-- <span class="price-shadow">30.490.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng chính hãng LG Việt Nam, bảo hành tại nhà</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel Core i5-1035G7 (1.2GHz up to 3.7GHz 6MB Cache)
                        <br>

                        Ram: 8GB DDR4 2400MHz Onboard (Còn 1 khe ram, up to 24GB SDRAM)
                        <br>

                        Ổ cứng: 512GB PCIe® NVMe™ M.2 SSD (còn trống 1 khe SSD)
                        <br>

                        Display: 14" FHD (1920 x 1080) IPS, sRGB 96%, 300nits
                        <br>

                        VGA: Intel® Iris® Plus Graphics
                        <br>

                        Pin: 4 Cell 72WHr (9,450 mAh)
                        <br>

                        Color: Dark Silver
                        <br>

                        Weight: 999 g
                        <br>

                        OS: Free DOS<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        Hoàn tiền 3.000.000Đ (Chưa trừ vào giá) HOẶC Set quà trị giá 6.000.000Đ bao gồm (túi chống shock thời trang LG + 1 chuột pebble Logitech + 1 ổ cứng WD SSD 512GB)<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/asus-zenbook-ux334fl-30-a4053t-white.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5095_asus_zenbook_edition_30_ux334_anh1.jpg" data-src="/media/product/250_5095_asus_zenbook_edition_30_ux334_anh1.jpg" alt="ASUS Zenbook UX334FL-30-A4053T White" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">28.890.000</span>
                        <!-- <span class="price-shadow">28.890.000</span> -->
    </span>


                    <span class="p-old-price">35.990.000 VND</span>
                    <span class="p-discount">-20%</span>

                    <a href="/asus-zenbook-ux334fl-30-a4053t-white.html" class="p-name">ASUS Zenbook UX334FL-30-A4053T White</a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(5095,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/asus-zenbook-ux334fl-30-a4053t-white.html" class="hover_name">ASUS Zenbook UX334FL-30-A4053T White</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">28.890.000</span>
              <!-- <span class="price-shadow">28.890.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
            <span class="marketPrice">
                <i>35.990.000</i>
            </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>24 Tháng chính hãng Asus Việt Nam</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel® Core™ i5-8265U (1.60GHz up to 3.90GHz, 4 nhân, 8 luồng, 6MB)
                        <br>

                        RAM: 8GB LPDDR3 2133 MHz
                        <br>

                        Ổ cứng: 512GB SSD M.2 PCIe
                        <br>

                        VGA: NVIDIA® GeForce® MX250 2GB GDDR5
                        <br>

                        Display: 13.3" FHD (1920 x 1080) IPS, Anti-Glare, NanoEdge, 100% sRGB
                        <br>

                        Backlit Keyboard
                        <br>

                        Bàn di chuột thông minh (Màn hình Super IPS FHD+ (2160 x 1080) 5,65 inch)
                        <br>

                        Pin: 3 Cell 50WHr Lithium-Polymer
                        <br>

                        Weight: 1.22Kg
                        <br>

                        Color: White
                        <br>

                        OS: Windows 10 Home SL 64bit.<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        - Bao da Asus + Chuột Asus + Bàn di chuột (Đi kèm trong máy)
                        - Balo thời trang cao cấp trị giá 490.000Đ<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/msi-modern-14-a10rb-888vn.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5721_" data-src="/media/product/250_5721_" alt="MSI Modern 14 A10RB-888VN " data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">22.490.000</span>
                        <!-- <span class="price-shadow">22.490.000</span> -->
    </span>


                    <span class="p-old-price">23.990.000 VND</span>
                    <span class="p-discount">-7%</span>

                    <a href="/msi-modern-14-a10rb-888vn.html" class="p-name">MSI Modern 14 A10RB-888VN </a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(5721,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/msi-modern-14-a10rb-888vn.html" class="hover_name">MSI Modern 14 A10RB-888VN </a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">22.490.000</span>
              <!-- <span class="price-shadow">22.490.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
            <span class="marketPrice">
                <i>23.990.000</i>
            </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng chính hãng MSI Việt Nam</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        NPP: VX
                        <br>

                        CPU: Intel Core i7-10510U (1.8GHz up to 4.9GHz 8MB Cache)
                        <br>

                        Ram: 8GB DDR4 2666MHz
                        <br>

                        Ổ cứng: 512GB PCIe NVMe™ M.2 SSD
                        <br>

                        VGA: NVIDIA GeForce MX250 2GB GDDR5
                        <br>

                        Display: 14" FHD (1920 x 1080) IPS-Level, 60Hz, 72% NTSC, Thin Bezel, 100% sRGB
                        <br>

                        Pin: 4 Cell 50WHr
                        <br>

                        Weight: 1.19 kg
                        <br>

                        Color: Carbon Grey
                        <br>

                        OS: Windows 10 Home bản quyền<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        - Túi bọc Sleeve Bag
                        - Chuột không dây Xtech 7073
                        - Bàn di chuột.<br>

                        "Check-in" Facebook Nhận ngay Tai nghe Wangming 9600 trị giá 290.000Đ (Từ 01/05 - 31/05/2020)<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/asus-zenbook-ux481fl-bm048t.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5042_asus_zenbook_ux481fl_anh3.jpg" data-src="/media/product/250_5042_asus_zenbook_ux481fl_anh3.jpg" alt="Asus ZenBook UX481FL-BM048T" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">28.490.000</span>
                        <!-- <span class="price-shadow">28.490.000</span> -->
    </span>


                    <span class="p-old-price">32.990.000 VND</span>
                    <span class="p-discount">-14%</span>

                    <a href="/asus-zenbook-ux481fl-bm048t.html" class="p-name">Asus ZenBook UX481FL-BM048T</a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(5042,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/asus-zenbook-ux481fl-bm048t.html" class="hover_name">Asus ZenBook UX481FL-BM048T</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">28.490.000</span>
              <!-- <span class="price-shadow">28.490.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
            <span class="marketPrice">
                <i>32.990.000</i>
            </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>24 Tháng chính hãng Asus Việt Nam</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel® Core™ i5-10210U (1.6GHz up to 4.2GHz, 4 nhân 8 luồng, 6MB Cache)
                        <br>

                        Ram: 8GB LPDDR3 2133MHz
                        <br>

                        Ổ cứng: 512GB PCIe SSD
                        <br>

                        VGA: NVIDIA GeForce MX250 2GB GDDR5
                        <br>

                        Display: 14.0" Full HD (1920 x 1080)
                        <br>

                        Color: Celestial Blue
                        <br>

                        Pin: 4-cell 70Whr
                        <br>

                        Weight: 1.5kg
                        <br>

                        Tính năng: ScreenPad Plus - Màn hình cảm ứng FHD 12.6"
                        <br>

                        OS: Windows 10 SL
                        <br>

                        Đi kèm bút trong hộp
                        <br>

                        Được vinh danh ở hạng mục "Laptop xuất sắc" tại Tech Awards 2019<br>

                    </div>


                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/dell-inspiron-15-g3-3590-n5i5518w.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_4784_" data-src="/media/product/250_4784_" alt="Dell Inspiron 15 G3 3590 N5I5518W" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">23.390.000</span>
                        <!-- <span class="price-shadow">23.390.000</span> -->
    </span>


                    <span class="p-old-price">25.990.000 VND</span>
                    <span class="p-discount">-11%</span>

                    <a href="/dell-inspiron-15-g3-3590-n5i5518w.html" class="p-name">Dell Inspiron 15 G3 3590 N5I5518W</a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(4784,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/dell-inspiron-15-g3-3590-n5i5518w.html" class="hover_name">Dell Inspiron 15 G3 3590 N5I5518W</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">23.390.000</span>
              <!-- <span class="price-shadow">23.390.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
            <span class="marketPrice">
                <i>25.990.000</i>
            </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 tháng chính hãng Dell Việt Nam, bảo hành tại nhà</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel® Core™ i5-9300H (2.40GHz up to 4.10GHz, 4 nhân 8 luồng, 8MB Cache)
                        <br>

                        Ram: 8GB DDR4 2666MHz, 2 slot Ram, Max 32GB
                        <br>

                        Ổ cứng: 512GB SSD M.2 PCIe3x4
                        <br>

                        VGA: NVIDIA Geforce GTX 1650 with 4GB GDDR5
                        <br>

                        Display: 15.6" FHD (1920 x 1080) IPS, Anti-Glare
                        <br>

                        Bàn phím: Backlit full-sized chiclet keyboard
                        <br>

                        Pin: 3-Cell 56WHrs
                        <br>

                        Weight: 2.34Kg
                        <br>

                        Os: Windows 10 bản quyền<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        - Túi xách Dell + Chuột Gaming Xtech TM - G8B + Bàn di chuột.<br>

                        "Check-in" Facebook Nhận ngay Tai nghe Wangming 9600 trị giá 290.000Đ (Từ 01/05 - 31/05/2020)<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/lg-gram-2020-15zd90n-v.ax56a5.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5759_15zd90n_v_ax56a5.jpg" data-src="/media/product/250_5759_15zd90n_v_ax56a5.jpg" alt="LG Gram 2020 15ZD90N-V.AX56A5" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">31.990.000</span>
                        <!-- <span class="price-shadow">31.990.000</span> -->
    </span>

                    <span class="p-old-price"></span>

                    <a href="/lg-gram-2020-15zd90n-v.ax56a5.html" class="p-name">LG Gram 2020 15ZD90N-V.AX56A5</a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(5759,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/lg-gram-2020-15zd90n-v.ax56a5.html" class="hover_name">LG Gram 2020 15ZD90N-V.AX56A5</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">31.990.000</span>
              <!-- <span class="price-shadow">31.990.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng chính hãng LG Việt Nam, bảo hành tại nhà</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel Core i5-1035G7 (1.2GHz up to 3.7GHz 6MB Cache)
                        <br>

                        Ram: 8GB DDR4 2400MHz Onboard (Còn 1 khe ram, up to 24GB SDRAM)
                        <br>

                        Ổ cứng: 512GB PCIe® NVMe™ M.2 SSD (còn trống 1 khe SSD)
                        <br>

                        Display: 15.6" FHD (1920 x 1080) IPS, sRGB 96%, 300nits
                        <br>

                        VGA: Intel® Iris® Plus Graphics
                        <br>

                        Pin: 2Cell 80WHr (10,336 mAh)
                        <br>

                        Color: White
                        <br>

                        Weight: 1120g
                        <br>

                        OS: Free DOS<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        Hoàn tiền 1.600.000Đ (Chưa trừ vào giá)
                        Set quà trị giá 6.000.000Đ (Từ 01/05 - 31/05/2020)  bao gồm:
                        - 1 túi chống shock thời trang LG
                        - 1 chuột pebble Logitech
                        - 1 ổ cứng WD SSD 512 GB<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/dell-inspiron-15-g3-3590-70191515.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_4783_dell_inspiron_15_g3_3590_anh1.jpg" data-src="/media/product/250_4783_dell_inspiron_15_g3_3590_anh1.jpg" alt="Dell Inspiron 15 G3 3590 70191515" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">29.190.000</span>
                        <!-- <span class="price-shadow">29.190.000</span> -->
    </span>


                    <span class="p-old-price">31.990.000 VND</span>
                    <span class="p-discount">-9%</span>

                    <a href="/dell-inspiron-15-g3-3590-70191515.html" class="p-name">Dell Inspiron 15 G3 3590 70191515</a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(4783,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/dell-inspiron-15-g3-3590-70191515.html" class="hover_name">Dell Inspiron 15 G3 3590 70191515</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">29.190.000</span>
              <!-- <span class="price-shadow">29.190.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
            <span class="marketPrice">
                <i>31.990.000</i>
            </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 tháng chính hãng Dell Việt Nam, bảo hành tại nhà</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel® Core™ i7-9750H (2.6GHz up to 4.5GHz, 6Cores, 12Threads, 12MB cache, FSB 8GT/s)
                        <br>

                        Ram: 8GB DDR4 2666MHz, 2 slot Ram, Max 32GB
                        <br>

                        Ổ cứng: 512GB SSD M.2 PCIe3x4
                        <br>

                        VGA: NVIDIA Geforce GTX 1660Ti 6GB GDDR6
                        <br>

                        Display: 15.6" FHD (1920 x 1080) IPS, Anti-Glare
                        <br>

                        Bàn phím: Backlit full-sized chiclet keyboard (RGB Full-color)
                        <br>

                        Pin: 3-Cell 56WHrs
                        <br>

                        Weight: 2.34Kg
                        <br>

                        Os: Windows 10 bản quyền<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        - Túi xách Dell + Chuột Gaming Xtech TM - G8B + Bàn di chuột.<br>

                        "Check-in" Facebook Nhận ngay Tai nghe Wangming 9600 trị giá 290.000Đ (Từ 01/05 - 31/05/2020)<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/lenovo-thinkpad-e15-20rds0dm00.html" class="p-img">
                        <img class="lazy loading" src="/template/default/images/blank.png" data-src="" alt="Lenovo ThinkPad E15 20RDS0DM00" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">15.790.000</span>
                        <!-- <span class="price-shadow">15.790.000</span> -->
    </span>

                    <span class="p-old-price"></span>

                    <a href="/lenovo-thinkpad-e15-20rds0dm00.html" class="p-name">Lenovo ThinkPad E15 20RDS0DM00</a>
                    <div class="p-bottom">

                        <span class="stock outstock"><i class="fa fa-check"></i> Liên hệ</span>

                        <span class="p-buy" onclick="listenBuyProduct(5998,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/lenovo-thinkpad-e15-20rds0dm00.html" class="hover_name">Lenovo ThinkPad E15 20RDS0DM00</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">15.790.000</span>
              <!-- <span class="price-shadow">15.790.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng chính hãng Lenovo Việt Nam</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Liên hệ</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel® Core™ i5-10210U (1.60GHz up to 4.20GHz, 6MB)
                        <br>

                        Ram: 1 x 8GB DDR4 2666MHz (1 slots, tối đa 16GB)
                        <br>

                        Ổ cứng: 256GB SSD PCIe (M.2 2242) – combo M.2 2242/ 2280
                        <br>

                        Display: 15.6 inch FHD (1920 x 1080) IPS Anti-glare
                        <br>

                        Pin: 3Cell, 45Wh
                        <br>

                        Weight: 1.8 kg
                        <br>

                        Tính năng: Bảo mật vân tay,…
                        <br>

                        OS: Free DOS<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        - Tặng Balo Lenovo chính hãng + Chuột không dây Xtech TM-G68 + Bàn di chuột.<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/dell-inspiron-15-g3-3590-i5-/8g-ram/-1t-hdd/-gtx-1050ti.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5984_" data-src="/media/product/250_5984_" alt="Dell Inspiron 15 G3 3590 (i5 /8G Ram/ 1T HDD/ GTX 1050Ti)" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">20.590.000</span>
                        <!-- <span class="price-shadow">20.590.000</span> -->
    </span>

                    <span class="p-old-price"></span>

                    <a href="/dell-inspiron-15-g3-3590-i5-/8g-ram/-1t-hdd/-gtx-1050ti.html" class="p-name">Dell Inspiron 15 G3 3590 (i5 /8G Ram/ 1T HDD/ GTX 1050Ti)</a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(5984,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/dell-inspiron-15-g3-3590-i5-/8g-ram/-1t-hdd/-gtx-1050ti.html" class="hover_name">Dell Inspiron 15 G3 3590 (i5 /8G Ram/ 1T HDD/ GTX 1050Ti)</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">20.590.000</span>
              <!-- <span class="price-shadow">20.590.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 tháng tại công ty Hoàng Minh Tech Co</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel® Core™ i5-9300H (2.40GHz up to 4.10GHz, 4 nhân 8 luồng, 8MB Cache)
                        <br>

                        Ram: 8GB DDR4 2666MHz, 2 slot Ram, Max 32GB
                        <br>

                        Ổ cứng: HDD 1TB 5400 Hybrid with 8GB Cache + 120 GB SSD ( Quà tặng Khuyến mại )
                        <br>

                        VGA: NVIDIA Geforce GTX 1050Ti with 3GB GDDR5
                        <br>

                        Display: 15.6" FHD (1920 x 1080) IPS, Anti-Glare
                        <br>

                        Bàn phím: Backlit full-sized chiclet keyboard (Blue-color)
                        <br>

                        Pin: 3-Cell 51WHrs
                        <br>

                        Weight: 2.34Kg
                        <br>

                        Os: Windows 10 bản quyền<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        - Túi xách Dell + Chuột Gaming Xtech TM - G8B + Bàn di chuột.<br>

                        Tặng ổ cứng SSD 120GB Adata trị giá 590.000Đ (Từ ngày 01/05 - 31/05/2020)<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/dell-latitude-7400-70194805.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5972_" data-src="/media/product/250_5972_" alt="Dell Latitude 7400 70194805" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">31.590.000</span>
                        <!-- <span class="price-shadow">31.590.000</span> -->
    </span>

                    <span class="p-old-price"></span>

                    <a href="/dell-latitude-7400-70194805.html" class="p-name">Dell Latitude 7400 70194805</a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(5972,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/dell-latitude-7400-70194805.html" class="hover_name">Dell Latitude 7400 70194805</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">31.590.000</span>
              <!-- <span class="price-shadow">31.590.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 tháng chính hãng Dell Việt Nam, bảo hành tại nhà</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel® Core™ i7-8665U (1.90 GHz upto 4.80 GHz, 8MB Cache)
                        <br>

                        Ram: 8GB(1x8GB) DDR4 ( còn 1 khe cắm Ram)
                        <br>

                        Ổ cứng: M.2 256GB PCIe NVMe SSD
                        <br>

                        VGA: Intel UHD Graphics 620
                        <br>

                        Display: 14.0" FHD (1920 x 1080) AG, Super Low Power LCD
                        <br>

                        Color: Black
                        <br>

                        Pin: 4 Cells ,60 Whrs
                        <br>

                        Cân nặng: 1.36Kg
                        <br>

                        OS: Linux<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        Túi xách Dell + Chuột không dây cao cấp Xtech TM-G68 + Bàn di chuột.<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/dell-inspiron-3493-n4i5122wa-silver.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5971_dell_inspiron_3493_1.jpg" data-src="/media/product/250_5971_dell_inspiron_3493_1.jpg" alt="Dell Inspiron 3493 N4I5122WA Silver" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">14.890.000</span>
                        <!-- <span class="price-shadow">14.890.000</span> -->
    </span>

                    <span class="p-old-price"></span>

                    <a href="/dell-inspiron-3493-n4i5122wa-silver.html" class="p-name">Dell Inspiron 3493 N4I5122WA Silver</a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(5971,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/dell-inspiron-3493-n4i5122wa-silver.html" class="hover_name">Dell Inspiron 3493 N4I5122WA Silver</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">14.890.000</span>
              <!-- <span class="price-shadow">14.890.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 tháng chính hãng Dell Việt Nam, bảo hành tại nhà</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel® Core™ i5-1035G1 (1.00 GHz up to 3.60 GHz, 6MB Cache)
                        <br>

                        Ram: 8GB DDR4 2666Mhz ( còn 1 khe cắm Ram)
                        <br>

                        Ổ cứng: 256GB SSD M2 PCIe NVMe
                        <br>

                        VGA: Intel® UHD Graphics
                        <br>

                        Display: 14.0" FHD (1920 x 1080) Anti-Glare LED Backlit Non-touch WVA Display None
                        <br>

                        Color: Silver
                        <br>

                        Pin: 3Cell 42Whr
                        <br>

                        Weight: 1.66kg
                        <br>

                        OS: Windows 10 Home SL bản quyền<br>

                    </div>


                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/acer-swift-3-sf314-42-r5z6-nx.hsesv.001-silver.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5970_" data-src="/media/product/250_5970_" alt="Acer Swift 3 SF314-42-R5Z6 NX.HSESV.001 Silver" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">17.590.000</span>
                        <!-- <span class="price-shadow">17.590.000</span> -->
    </span>

                    <span class="p-old-price"></span>

                    <a href="/acer-swift-3-sf314-42-r5z6-nx.hsesv.001-silver.html" class="p-name">Acer Swift 3 SF314-42-R5Z6 NX.HSESV.001 Silver</a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(5970,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/acer-swift-3-sf314-42-r5z6-nx.hsesv.001-silver.html" class="hover_name">Acer Swift 3 SF314-42-R5Z6 NX.HSESV.001 Silver</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">17.590.000</span>
              <!-- <span class="price-shadow">17.590.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng chính hãng Acer Việt Nam, gói bảo hành VIP 3S1</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel Ryzen 5-4500U (2.30GHz upto 4.00GHz, 8MB Cache)
                        <br>

                        RAM: 8GB DDR4 2400MHz
                        <br>

                        Ổ cứng: 512GB PCIe NVMe SSD M2
                        <br>

                        VGA: AMD Radeon™ Graphics
                        <br>

                        Display: 14.0"Full HD (1920x1080) IPS
                        <br>

                        Finger Print / Color: Silver
                        <br>

                        Pin : 4 cell
                        <br>

                        Weight: 1.19Kg
                        <br>

                        OS : Windows 10 Home 64 bit bản quyền<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        - Balo Acer + Chuột không dây Xtech TM-G68 + Bàn di chuột + Gói bảo hành 3S1.<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/acer-aspire-a515-55-55ja-nx.hsmsv.003.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5969_" data-src="/media/product/250_5969_" alt=" Acer Aspire A515-55-55JA NX.HSMSV.003" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">15.790.000</span>
                        <!-- <span class="price-shadow">15.790.000</span> -->
    </span>

                    <span class="p-old-price"></span>

                    <a href="/acer-aspire-a515-55-55ja-nx.hsmsv.003.html" class="p-name"> Acer Aspire A515-55-55JA NX.HSMSV.003</a>
                    <div class="p-bottom">

                        <span class="stock outstock"><i class="fa fa-check"></i> Liên hệ</span>

                        <span class="p-buy" onclick="listenBuyProduct(5969,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/acer-aspire-a515-55-55ja-nx.hsmsv.003.html" class="hover_name"> Acer Aspire A515-55-55JA NX.HSMSV.003</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">15.790.000</span>
              <!-- <span class="price-shadow">15.790.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng chính hãng Acer Việt Nam</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Liên hệ</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel® Core™ i5-1035G1 (1.00GHz up to 3.60GHz, 6MB Cache)
                        <br>

                        RAM: 8GB DDR4 2666MHz (Max 16GB)
                        <br>

                        Ổ cứng: 512GB PCIe NVMe SSD (nâng cấp tối đa 2TB HDD và 512GB PCIe NVMe)
                        <br>

                        VGA: Intel UHD Graphics 620
                        <br>

                        Display: 15.6" FHD (1920x1080) Acer ComfyView LED LCD
                        <br>

                        Pin: 4-cell 48Wh
                        <br>

                        Weight: 1.70kg
                        <br>

                        Color: Pure Silver
                        <br>

                        OS: Windows 10 Home bản quyền
                        <br>

                        Tính năng: LED_KeyBoard<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        - Balo chính hãng Acer + Chuột không dây Xtech TM-G68 + Bàn di chuột.<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/lenovo-ideapad-l340-15irh-81lk01j2vn.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5968_" data-src="/media/product/250_5968_" alt="Lenovo IdeaPad L340-15IRH 81LK01J2VN" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">19.490.000</span>
                        <!-- <span class="price-shadow">19.490.000</span> -->
    </span>


                    <span class="p-old-price">19.990.000 VND</span>
                    <span class="p-discount">-3%</span>

                    <a href="/lenovo-ideapad-l340-15irh-81lk01j2vn.html" class="p-name">Lenovo IdeaPad L340-15IRH 81LK01J2VN</a>
                    <div class="p-bottom">

                        <span class="stock outstock"><i class="fa fa-check"></i> Liên hệ</span>

                        <span class="p-buy" onclick="listenBuyProduct(5968,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/lenovo-ideapad-l340-15irh-81lk01j2vn.html" class="hover_name">Lenovo IdeaPad L340-15IRH 81LK01J2VN</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">19.490.000</span>
              <!-- <span class="price-shadow">19.490.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
            <span class="marketPrice">
                <i>19.990.000</i>
            </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng chính hãng Lenovo Việt Nam</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Liên hệ</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel® Core™ i5-9300H (2.40GHz up to 4.10GHz, 4 nhân 8 luồng, 8MB Cache)
                        <br>

                        RAM: 8GB DDR4 2666MHz (1x SO-DIMM socket, up to 16GB SDRAM)
                        <br>

                        Ổ cứng: 512GB SSD M.2 PCIE
                        <br>

                        VGA: NVIDIA GeForce GTX 1650 4GB GDDR5
                        <br>

                        Display: 15.6 inch FHD (1920x1080) IPS 250nits Anti-glare
                        <br>

                        Pin: 3Cell, 45WH
                        <br>

                        Weight: 2.19kg
                        <br>

                        OS: Windows 10 Home 64bit bản quyền<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        - Tặng Balo Lenovo chính hãng + Chuột không dây Xtech TM-G68 + Bàn di chuột.<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/dell-inspiron-3593-70211828-silver.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5960_" data-src="/media/product/250_5960_" alt="Dell Inspiron 3593 70211828 Silver" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">21.590.000</span>
                        <!-- <span class="price-shadow">21.590.000</span> -->
    </span>


                    <span class="p-old-price">22.990.000 VND</span>
                    <span class="p-discount">-7%</span>

                    <a href="/dell-inspiron-3593-70211828-silver.html" class="p-name">Dell Inspiron 3593 70211828 Silver</a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(5960,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/dell-inspiron-3593-70211828-silver.html" class="hover_name">Dell Inspiron 3593 70211828 Silver</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">21.590.000</span>
              <!-- <span class="price-shadow">21.590.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
            <span class="marketPrice">
                <i>22.990.000</i>
            </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 tháng chính hãng Dell Việt Nam, bảo hành tại nhà</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel® Core™ i7-1065G7 (1.30 GHz up to 3.90 GHz, 8MB Cache)
                        <br>

                        RAM: 8GB DDR4 ( còn 1 khe cắm Ram)
                        <br>

                        Ổ cứng: 512GB SSD
                        <br>

                        VGA: Nvidia GeForce MX230 2GB
                        <br>

                        Display: 15.6" FHD (1920*1080)
                        <br>

                        Color: Silver
                        <br>

                        Pin: 4 cell
                        <br>

                        Cân nặng: 2.2kg
                        <br>

                        OS: Windows 10 Home bản quyền<br>

                    </div>


                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/asus-x509jp-ej012t-silver.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5933_" data-src="/media/product/250_5933_" alt="ASUS X509JP-EJ012T Silver" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">14.390.000</span>
                        <!-- <span class="price-shadow">14.390.000</span> -->
    </span>


                    <span class="p-old-price">14.990.000 VND</span>
                    <span class="p-discount">-5%</span>

                    <a href="/asus-x509jp-ej012t-silver.html" class="p-name">ASUS X509JP-EJ012T Silver</a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(5933,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/asus-x509jp-ej012t-silver.html" class="hover_name">ASUS X509JP-EJ012T Silver</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">14.390.000</span>
              <!-- <span class="price-shadow">14.390.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
            <span class="marketPrice">
                <i>14.990.000</i>
            </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>24 Tháng chính hãng Asus Việt Nam</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel® Core™ i5-1035G1 (1.00 GHz up to 3.60 GHz, 6MB Cache)
                        <br>

                        RAM: 4GB DDR4 bus 2400MHz (Còn 1 slot ram)
                        <br>

                        Ổ cứng:  1TB 5400rpm + 1slot M2 SSD
                        <br>

                        VGA: NVIDIA GeForce MX330 2GB GDDR5
                        <br>

                        Display: 15.6" FHD (1920 x 1080) Anti-Glare with 45% NTSC
                        <br>

                        Finger Print, Bluetooth v5.0/ USB / HD Camera
                        <br>

                        Weigh: 1.8kg
                        <br>

                        Pin: 2cell 32 WHrs
                        <br>

                        Color: Silver
                        <br>

                        OS: Windows 10 SL bản quyền<br>

                    </div>


                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/lenovo-ideapad-s145-15api-81ut00f1vn.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5932_lenovo_ideapad_s145_1.png" data-src="/media/product/250_5932_lenovo_ideapad_s145_1.png" alt="Lenovo IdeaPad S145-15API 81UT00F1VN" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">11.490.000</span>
                        <!-- <span class="price-shadow">11.490.000</span> -->
    </span>

                    <span class="p-old-price"></span>

                    <a href="/lenovo-ideapad-s145-15api-81ut00f1vn.html" class="p-name">Lenovo IdeaPad S145-15API 81UT00F1VN</a>
                    <div class="p-bottom">

                        <span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>

                        <span class="p-buy" onclick="listenBuyProduct(5932,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/lenovo-ideapad-s145-15api-81ut00f1vn.html" class="hover_name">Lenovo IdeaPad S145-15API 81UT00F1VN</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">11.490.000</span>
              <!-- <span class="price-shadow">11.490.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng chính hãng Lenovo Việt Nam</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Còn hàng</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: AMD Ryzen 5-3500U (2.10Ghz upto 3.70Ghz, 4MB)
                        <br>

                        Ram: 4GB DDR4 2400MHz (1 khe)
                        <br>

                        Ổ cứng: 512GB SSD M.2 2242 NVMe
                        <br>

                        VGA: Radeon™ Vega 8 Graphics
                        <br>

                        Display: 15.6 FHD (1920x1080) anti-glare, LED backlight
                        <br>

                        Pin: 2Cell, 35WH
                        <br>

                        Weight: 1.85kg
                        <br>

                        OS: Windows 10 Home bản quyền<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        - Tặng Balo Lenovo chính hãng + Chuột không dây Xtech TM-G68 + Bàn di chuột.<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/laptop-fujitsu-lifebook-u939-l00u939vn00000261.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5931_" data-src="/media/product/250_5931_" alt="Laptop Fujitsu Lifebook U939 'L00U939VN00000261 " data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">42.590.000</span>
                        <!-- <span class="price-shadow">42.590.000</span> -->
    </span>


                    <span class="p-old-price">46.090.000 VND</span>
                    <span class="p-discount">-8%</span>

                    <a href="/laptop-fujitsu-lifebook-u939-l00u939vn00000261.html" class="p-name">Laptop Fujitsu Lifebook U939 'L00U939VN00000261 </a>
                    <div class="p-bottom">

                        <span class="stock outstock"><i class="fa fa-check"></i> Liên hệ</span>

                        <span class="p-buy" onclick="listenBuyProduct(5931,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/laptop-fujitsu-lifebook-u939-l00u939vn00000261.html" class="hover_name">Laptop Fujitsu Lifebook U939 'L00U939VN00000261 </a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">42.590.000</span>
              <!-- <span class="price-shadow">42.590.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
            <span class="marketPrice">
                <i>46.090.000</i>
            </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Liên hệ</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel® Core™ i7-8565U (1.80GHz up to 4.60GHz, 8MB Cache)
                        <br>

                        Ram: 16GB LPDDR3 2133 MHz (on board)
                        <br>

                        Ổ cứng: 512GB SSD M.2 SATA
                        <br>

                        VGA: Intel® UHD Graphics 620
                        <br>

                        Display: 13.3 inch FHD (1920 x 1080) antiglare - Cảm ứng
                        <br>

                        Pin: 4Cell, 50WH
                        <br>

                        Weight: 916 g
                        <br>

                        OS: Free Dos
                        <br>

                        Made in Japan<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        - Tặng 01 Gift Fujitsu Travel Adapter TA100-PD trị giá 700.000Đ (Từ 01/04 - 30/06/2020)<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/laptop-fujitsu-lifebook-u939-l00u939vn00000260.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5930_fujitsu_lifebook_u939__1.jpg" data-src="/media/product/250_5930_fujitsu_lifebook_u939__1.jpg" alt="Laptop Fujitsu Lifebook U939 L00U939VN00000260" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">35.190.000</span>
                        <!-- <span class="price-shadow">35.190.000</span> -->
    </span>


                    <span class="p-old-price">37.990.000 VND</span>
                    <span class="p-discount">-8%</span>

                    <a href="/laptop-fujitsu-lifebook-u939-l00u939vn00000260.html" class="p-name">Laptop Fujitsu Lifebook U939 L00U939VN00000260</a>
                    <div class="p-bottom">

                        <span class="stock outstock"><i class="fa fa-check"></i> Liên hệ</span>

                        <span class="p-buy" onclick="listenBuyProduct(5930,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/laptop-fujitsu-lifebook-u939-l00u939vn00000260.html" class="hover_name">Laptop Fujitsu Lifebook U939 L00U939VN00000260</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">35.190.000</span>
              <!-- <span class="price-shadow">35.190.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
            <span class="marketPrice">
                <i>37.990.000</i>
            </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Liên hệ</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel® Core™ i5-8265U (1.60GHz upto 3.90GHz, 6MB)
                        <br>

                        Ram: 8GB LPDDR3 2133 MHz (on board)
                        <br>

                        Ổ cứng: 512GB SSD M.2 SATA
                        <br>

                        VGA: Intel® UHD Graphics 620
                        <br>

                        Display: 13.3 inch FHD (1920 x 1080) antiglare - Cảm ứng
                        <br>

                        Pin: 4Cell, 50WH
                        <br>

                        Weight: 916 g
                        <br>

                        OS: Free Dos
                        <br>

                        Made in Japan<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        - Tặng 01 Gift Fujitsu Travel Adapter TA100-PD trị giá 700.000Đ (Từ 01/04 - 30/06/2020)<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/laptop-fujitsu-lifebook-u729-l00u729vn00000092.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5928_" data-src="/media/product/250_5928_" alt="Laptop Fujitsu Lifebook U729 L00U729VN00000092" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">31.590.000</span>
                        <!-- <span class="price-shadow">31.590.000</span> -->
    </span>


                    <span class="p-old-price">34.190.000 VND</span>
                    <span class="p-discount">-8%</span>

                    <a href="/laptop-fujitsu-lifebook-u729-l00u729vn00000092.html" class="p-name">Laptop Fujitsu Lifebook U729 L00U729VN00000092</a>
                    <div class="p-bottom">

                        <span class="stock outstock"><i class="fa fa-check"></i> Liên hệ</span>

                        <span class="p-buy" onclick="listenBuyProduct(5928,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/laptop-fujitsu-lifebook-u729-l00u729vn00000092.html" class="hover_name">Laptop Fujitsu Lifebook U729 L00U729VN00000092</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">31.590.000</span>
              <!-- <span class="price-shadow">31.590.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
            <span class="marketPrice">
                <i>34.190.000</i>
            </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Liên hệ</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel® Core™ i7-8565U (1.80GHz up to 4.60GHz, 8MB Cache)
                        <br>

                        Ram: 8GB DDR4 2400MHz
                        <br>

                        Ổ cứng: 512GB SSD M.2 SATA
                        <br>

                        VGA: Intel® UHD Graphics 620
                        <br>

                        Display: 12.5 inch FHD (1920 x 1080) - Cảm ứng
                        <br>

                        Pin: 3Cell 45WH
                        <br>

                        Weight: 1.11 kg
                        <br>

                        OS: Free Dos
                        <br>

                        Tính năng: Bảo mật vân tay
                        <br>

                        Made in Japan<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        - Tặng 01 Gift Fujitsu Travel Adapter TA100-PD trị giá 700.000Đ (Từ 01/04 - 30/06/2020)<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

            <div class="p-item">
                <div class="p-container">
                    <a href="/laptop-fujitsu-lifebook-u749-l00u749vn00000114.html" class="p-img">
                        <img class="lazy loaded" src="/media/product/250_5927_" data-src="/media/product/250_5927_" alt="Laptop Fujitsu Lifebook U749 L00U749VN00000114" data-was-processed="true">
                    </a>

                    <span class="p-price">
      <span class="price-border">29.290.000</span>
                        <!-- <span class="price-shadow">29.290.000</span> -->
    </span>


                    <span class="p-old-price">31.690.000 VND</span>
                    <span class="p-discount">-8%</span>

                    <a href="/laptop-fujitsu-lifebook-u749-l00u749vn00000114.html" class="p-name">Laptop Fujitsu Lifebook U749 L00U749VN00000114</a>
                    <div class="p-bottom">

                        <span class="stock outstock"><i class="fa fa-check"></i> Liên hệ</span>

                        <span class="p-buy" onclick="listenBuyProduct(5927,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>
                    </div>
                </div>

                <div class="hover_content_pro tooltip">
                    <a href="/laptop-fujitsu-lifebook-u749-l00u749vn00000114.html" class="hover_name">Laptop Fujitsu Lifebook U749 L00U749VN00000114</a>
                    <table>
                        <tbody><tr>
                            <td colspan="2">
          <span class="p-price-full">
            <span class="price-border">29.290.000</span>
              <!-- <span class="price-shadow">29.290.000</span> -->
          </span>
                                <span class="hover_vat">


          </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
            <span class="marketPrice">
                <i>31.690.000</i>
            </span>
                            </td>
                        </tr>

                        <tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng</td></tr>
                        <tr>
                            <td><b>Kho hàng:</b></td>

                            <td><span>Liên hệ</span></td>

                        </tr>
                        </tbody></table>

                    <div class="hori_line"></div>
                    <div class="hover_summary">

                        <b>Mô tả tóm tắt:</b><br>

                        CPU: Intel® Core™ i7-8565U (1.80GHz up to 4.60GHz, 8MB Cache)
                        <br>

                        Ram: 8GB DDR4 2400MHz
                        <br>

                        Ổ cứng: 512GB SSD M.2 SATA
                        <br>

                        VGA: Intel® UHD Graphics 620
                        <br>

                        Display: 14.0 inch FHD (1920 x 1080)
                        <br>

                        Pin: 4cell 50WH
                        <br>

                        Weight: 1.49 kg
                        <br>

                        OS: Free Dos
                        <br>

                        Tính năng: Bảo mật vân tay,…
                        <br>

                        Made in Japan<br>

                    </div>


                    <div class="hori_line"></div>
                    <div class="hover_offer">
                        <b>Khuyến mại:</b><br>

                        - Tặng 01 Gift Fujitsu Travel Adapter TA100-PD trị giá 700.000Đ (Từ 01/04 - 30/06/2020)<br>

                    </div>

                </div><!--hover_content_pro-->
            </div><!--p-item-->

        </div><!--prouduct_list-->

        <div class="clearfix"></div>
        <div class="top_area_list_page mt-2">
            <div class="sort_style">
                <span>Lựa chọn</span>
                <a onclick="setUserOption('product_display', 'list', 'https://laptopworld.vn/may-tinh-xach-tay.html')" href="javascript:void(0)"><i class="fa fa-list"></i></a>
                <a class="active" onclick="setUserOption('product_display', 'grid', 'https://laptopworld.vn/may-tinh-xach-tay.html')" href="javascript:void(0)"><i class="fa fa-th-large"></i></a>
            </div>
            <div class="sort_pro">
                <select onchange="location.href=this.value">
                    <option value="">Sắp xếp sản phẩm</option>


                    <option value="https://laptopworld.vn/may-tinh-xach-tay.html?sort=new">Mới nhất</option>



                    <option value="https://laptopworld.vn/may-tinh-xach-tay.html?sort=price-asc">Giá tăng dần</option>



                    <option value="https://laptopworld.vn/may-tinh-xach-tay.html?sort=price-desc">Giá giảm dần</option>



                    <option value="https://laptopworld.vn/may-tinh-xach-tay.html?sort=view">Lượt xem</option>








                </select>
            </div>

            <div class="paging">

                <a href="#" class="current">1</a>

                <a href="/may-tinh-xach-tay.html?page=2">2</a>

                <a href="/may-tinh-xach-tay.html?page=3">3</a>

                <a href="/may-tinh-xach-tay.html?page=4">4</a>

                <a href="/may-tinh-xach-tay.html?page=5">5</a>

                <a href="/may-tinh-xach-tay.html?page=6">6</a>

                <a href="/may-tinh-xach-tay.html?page=7">7</a>

                <a href="/may-tinh-xach-tay.html?page=2">Next</a>

            </div><!--paging-->

        </div><!--top_area_list_page-->
    </div>
    @include('partials.home.content-right')
@endsection
