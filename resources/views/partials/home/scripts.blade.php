<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@11.0.6/dist/lazyload.min.js"></script>
<script src="/includes/js/common.js"></script>
<script src="/javascript/dist/hurastore.js?v=4"> </script>
<script src="/template/default/script/global.js?v=88999888"> </script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />

<script>
    $(".singleSlider").owlCarousel({
        items:1,
        margin:0,
        loop:true,
        nav:true,
        dots:false,
        navText:['<i class="icon icon-owl-prev"></i>','<i class="icon icon-owl-next"></i>']
    });
    function showCartNotify(){
        $("#cart-fixed").addClass("hover");
        setTimeout(function(){
            $("#cart-fixed").removeClass("hover");
        },2000);
    }
    $(window).scroll(function(){
        if($(window).scrollTop() > 525) $("nav,header").addClass("fixed");
        else $("nav,header").removeClass("fixed");
    });

    $(".title-filter-collapse").click(function(){
        $(this).toggleClass("active");
        $(this).parents(".filter").find(".content_box").toggle();

        if($(this).hasClass("active")) $(this).find(".icon").html("-");
        else $(this).find(".icon").html("+");
    });
</script>

<script>
    function getProductList(url, holder, limit, actionLoad) {
        $.getJSON(url, function(result) {
            var data = "";
            var html = "";

            if (typeof result.list !== 'undefined') data = result.list;
            else data = result;

            //console.log("data",data);
            data.forEach(function(item,index) {
                if(index > limit - 1) return;

                var price = item.price;
                var priceFormat = Hura.Util.writeStringToPrice(price);
                if (price == 0) priceFormat = "Liên hệ";
                var marketPrice = item.marketPrice;
                var discount = 0;
                if (marketPrice > price) {
                    discount = Math.ceil(100 - price * 100 / marketPrice);
                }

                var specialOffer = "";
                if (item.specialOffer.all.length > 0){
                    item.specialOffer.all.forEach(function(item){
                        specialOffer+=item.title+"<br>";
                    });

                }

                html+='<div class="p-item">';
                html+='<div class="p-container">';
                html+='<a href="'+item.productUrl+'" class="p-img">';
                html+='<img src="'+item.productImage.original+'" alt="'+item.productName+'">';
                html+='</a>';
                html+='<span class="p-price">';
                html+='<span class="price-border">'+priceFormat+'</span>';
                html+='<span class="price-shadow">'+priceFormat+'</span>';
                html+='</span>';
                if(discount > 0){
                    html+='<span class="p-old-price">'+Hura.Util.writeStringToPrice(marketPrice)+' VND</span>';
                    html+='<span class="p-discount">-'+discount+'%</span>';
                }else html+='<span class="p-old-price"></span>';
                html+='<a href="'+item.productUrl+'" class="p-name">'+item.productName+'</a>';
                html+='<div class="p-bottom">';
                if(item.quantity > 0) html+='<span class="stock instock"><i class="fa fa-check"></i> Có hàng</span>';
                else html+='<span class="stock outstock"><i class="fa fa-check"></i> Liên hệ</span>';
                html+='<span class="p-buy" onclick="listenBuyProduct('+item.productId+',0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span>';
                html+='</div>';
                html+='</div>';

                html += '<div class="hover_content_pro">';
                html += '<a href="' + item.productUrl + '" class="hover_name">' + item.productName + '</a>';
                html += '<table>';
                html += '<tr>';
                html += '<td colspan="2">';
                html+='<span class="p-price-full">';
                html+='<span class="price-border">'+priceFormat+'</span>';
                html+='<span class="price-shadow">'+priceFormat+'</span>';
                html+='</span>';
                html += '<span class="hover_vat">';
                if (item.hasVAT == 1 && price > 0) html += '[Đã bao gồm VAT]';
                if (item.hasVAT == 2 && price > 0) html += '[Chưa bao gồm VAT]';
                html += '</span>';
                html += '</td>';
                html += '</tr>';
                if (marketPrice > price) {
                    html += '<tr><td colspan="2">';
                    html += '<span class="marketPrice" style="font-size:16px; text-decoration:line-through"><span>' + formatCurrency(marketPrice) + '</span></span></td></tr>';
                }
                if(item.warranty!=0)
                    html += '<tr><td width="90"><b>Bảo hành:</b></td><td>' + item.warranty + '</td></tr>';
                else
                    html += '<tr><td width="90"><b>Bảo hành:</b></td><td>Liên hệ</td></tr>';
                html += '<tr>';
                html += '<td><b>Kho hàng:</b></td>';
                if (item.quantity > 0) html += '<td><b>Còn hàng</b></td>';
                else html += '<td><b>Liên hệ</b></td>';


                html += '</tr>';
                html += '</table>';
                if (item.productSummary != '') {
                    html += '<div class="hori_line"></div>';
                    html += '<div class="hover_summary">';
                    html += '<b>Mô tả tóm tắt:</b><br/>';
                    html += '<span style="white-space:pre-line;">' + item.productSummary + '</span>';
                    html += '</div>';
                }
                if (specialOffer!='' && 1 == 2){
                    html += '<div class="hori_line"></div>';
                    html += '<div class="hover_offer">';
                    html += '<b>Khuyến mại:</b><br/>';
                    html += '<span style="white-space:pre-line;">' + specialOffer + '</span>';
                    html += '</div>';
                }
                html += '</div>';

                html+='</div>';
            });

            if (typeof actionLoad === 'undefined') {
                actionLoad = 'replace';
            }

            if(html=='') return false;
            if (actionLoad == 'replace') $(holder).html(html);
            else $(holder).append(html);
        });
    }

    function getDealList(url, holder, limit, actionLoad) {
        $.getJSON(url, function(result) {
            var data = "";
            var html = "";
            var max_time = 0;

            if (typeof result.list !== 'undefined') data = result.list;
            else data = result;

            data.forEach(function(item,index) {
                if(index > limit - 1) return;

                var product_info = item.product_info;
                var price = parseInt(item.price);
                var priceFormat = Hura.Util.writeStringToPrice(price);
                if (price == 0) priceFormat = "Liên hệ";
                var marketPrice = parseInt(product_info.marketPrice);
                var discount = 0;
                if (marketPrice > price) {
                    discount = Math.ceil(100 - price * 100 / marketPrice);
                }
                var total_quantity = parseInt(product_info.quantity);
                var deal_quantity_left = parseInt(item.quantity);
                if(deal_quantity_left > total_quantity) deal_quantity_left = total_quantity;
                if(item.deal_time_left > max_time) max_time = item.deal_time_left



                html+='<div class="d-item">';
                html+='<div class="p-container">';
                html+='<a href="'+product_info.productUrl+'?deal='+item.id+'" class="p-img">';
                html+='<img src="'+product_info.productImage.original+'" alt="'+product_info.productName+'">';
                html+='</a>';
                html+='<span class="p-price">';
                html+='<span class="price-border">'+priceFormat+'</span>';
                html+='<span class="price-shadow">'+priceFormat+'</span>';
                html+='</span>';
                if(discount > 0){
                    html+='<span class="p-old-price">'+Hura.Util.writeStringToPrice(marketPrice)+' VND</span>';
                    html+='<span class="p-discount">-'+discount+'%</span>';
                }else html+='<span class="p-old-price"></span>';
                html+='<a href="'+product_info.productUrl+'?deal='+item.id+'" class="p-name">'+product_info.productName+'</a>';
                html+='<div class="p-order-status" data-left="'+deal_quantity_left+'" data-total="'+total_quantity+'">';
                html+='<span class="text">Còn lại '+deal_quantity_left+'</span>';
                html+='<span class="bg-count-left"></span>';
                html+='<span class="icon-order-status icon-order-status-deal"></span>';
                html+='</div>';
                html+='</div>';
                html+='</div>';
            });

            if (typeof actionLoad === 'undefined') {
                actionLoad = 'replace';
            }

            if(html=='') return false;
            if (actionLoad == 'replace') $(holder).html(html);
            else $(holder).append(html);

            if($("#js-time-dealgroup")) startClock(max_time,"","#js-time-dealgroup .h","#js-time-dealgroup .m","#js-time-dealgroup .s");
        });
    }
</script>

<script>
    //on ready
    $(function () {
        //hien thi tom tat cart
        var check_cart = setInterval(function () {
            if(showCartSummary()) clearInterval(check_cart);
        }, 3000);
    })


    //show cart summary
    function showCartSummary() {
        var $status_container = $(".shopping-cart-item");

        if(!Hura.Cart.isReady()) return false;

        var cart_summary = Hura.Cart.getSummary();
        //console.log("cart_summary = " + JSON.stringify(cart_summary, true, 4));
        $status_container.html(cart_summary.item);

        return true;
    }
</script>

<script>
    function addProductToCart(product_id, variant_id, quantity, buyer_note) {
        var product_prop = {
            quantity: quantity ,
            buyer_note : buyer_note
        };

        return Hura.Cart.Product.add(product_id, variant_id, product_prop);
    }

    function listenBuyProduct(product_id,variant_id,quantity, redirect) {
        if(!Hura.Cart.isReady()) alert("Cart chua san sang!");
        var product_id  = product_id;
        var variant_id  = variant_id;
        var buyer_note  = '';
        var quantity = quantity;

        var addStatus = addProductToCart(product_id, variant_id, quantity, buyer_note);
        if(addStatus) {
            addStatus.then(function () {
                console.log("cart content = " + JSON.stringify(Hura.Cart.getCart(), true, 4));
                showCartSummary();

                if (typeof redirect === 'undefined') {
                    alert("Đã thêm sản phẩm vào giỏ hàng");
                    showCartNotify();
                }else if(redirect == 'cart'){
                    location.href="/cart";
                }else if(redirect == 'payinstall'){
                    location.href="/cart?show=tragop&type=cart";
                }

            })
        } else {
            console.log("cart content = " + JSON.stringify(Hura.Cart.getCart(), true, 4));
            //Hura.Cart.goToCartPage();
        }
    }


    function listenBuyDeal(deal_id,quantity,redirect) {

        if(!Hura.Cart.isReady()) alert("Cart chua san sang!");


        var deal_info = {
            id : deal_id,
            quantity: 1,
            buyer_note : '',
        };
        var addStatus = Hura.Cart.Deal.add(deal_info);
        console.log("addStatus",addStatus);

        if(addStatus) {
            addStatus.then(function () {
                console.log("cart content = " + JSON.stringify(Hura.Cart.getCart(), true, 4));
                showCartSummary();

                if (typeof redirect === 'undefined') {
                    alert("Đã thêm sản phẩm vào giỏ hàng");
                    showCartNotify();
                }else if(redirect == 'cart'){
                    location.href="/cart";
                }
            })
        } else {
            console.log("cart content = " + JSON.stringify(Hura.Cart.getCart(), true, 4));
            //Hura.Cart.goToCartPage();
        }
    }
</script>


<!--check load content-->
<script>
    $(window).scroll(function(){
        if(isOnScreen($(".js-load-video")) && !$(".js-load-video").hasClass("loaded")){
            $(".js-load-video").html('<iframe width="560" height="315" src="https://www.youtube.com/embed/Iyc99HJvMo8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
            $(".js-load-video").addClass("loaded");
        }

        if(isOnScreen($("#brand-footer")) && !$("#brand-footer").hasClass("loaded")){
            var url = "/ajax/get_json.php?action=banner&action_type=list&template=footer&location=5&sort=order&show=20";
            var holder = "#brand-footer .owl-carousel";
            getBanner(url,5,holder,20);
            $("#brand-footer").addClass("loaded");

            $(document).ajaxStop(function(){
                $("#brand-footer .owl-carousel").owlCarousel({
                    items:9,
                    dots:false,
                    loop:true,
                    margin:10,
                    nav:true,
                    navText:['<i class="fa fa-chevron-circle-left"></i>','<i class="fa fa-chevron-circle-right"></i>']
                });
            });
        }
    });
</script>

<!--format price-->
<script>
    function formatPrice(){
        $(".p-price").each(function(){
            if(!$(this).hasClass("formated")){
                var price = $(this).find("span").html();
                if(price=='Liên hệ') return;
                var price_format = price.substring(0, parseInt(price.length) - 3);
                $(this).find("span").html(price_format);
                $(this).addClass("formated");
            }
        });
    }
    //formatPrice();
    $(document).ajaxStop(function(){
        //formatPrice();
    });
</script>

<script>
    function getBanner(url,location,holder,limit){
        var key = "location_"+location;
        var html = "";
        $.getJSON(url,function(data){
            console.log("data",data);
            if(data.length == 0) return;
            data[key].forEach(function(item,index){
                html+= "<div class='item'>"+item.display+"</div>";
            });
            $(holder).html(html);

        });
    }
</script>

<script>
    function autoSortSubMenu(wrap_item_new,wrap_item_original,col_number,plus){
        /*
        wrap_item_new: Box sẽ chứa các thành phần sau khi sắp xếp
        wrap_item_origian: Box chứa các thành phần trước khi sắp xếp, sắp xếp xong sẽ xóa box này đi
        col_number: số cột muốn hiển thị
        plus = 0 (không thay đổi, đây là sai số cộng thêm khi kiểm tra số link trong 1 box nhỏ)
        */
        var totalItem = $(wrap_item_original).find("a").length;
        var itemPerCol = Math.ceil(totalItem/col_number); //Số link trong 1 cột

        var totalBoxItemOriginal = $(wrap_item_original).find(".box_cate").length;
        var totalBoxItemNew = 0;
        //console.log("itemPerCol",totalItem,itemPerCol);
        if(totalItem > 0){
            var htmlCol = "";
            for(var i=0; i < col_number; i++){
                htmlCol+='<div class="col_sub_item_menu"></div>';
            }
            $(wrap_item_new).html(htmlCol);
        }
        var currentCol = 0;
        $(wrap_item_original + " div").each(function () {
            var this_total_item = $(this).find("a").length;
            if($(wrap_item_new).find(".col_sub_item_menu").eq(currentCol).find("a").length + this_total_item <= itemPerCol+plus){
                $(wrap_item_new).find(".col_sub_item_menu").eq(currentCol).append('<div class="box">'+$(this).html()+'</div>');
                totalBoxItemNew++;
            }else if(currentCol < col_number - 1){
                currentCol++;
                totalBoxItemNew++;
                $(wrap_item_new).find(".col_sub_item_menu").eq(currentCol).append('<div class="box">'+$(this).html()+'</div>');
            }else {
                //$(".col_sub_item_menu").eq(currentCol).append($(this).html());
            }
        });
        //console.log(plus);
        //Nếu không sắp xếp đủ thì tăng sai số
        if(totalBoxItemNew < totalBoxItemOriginal) autoSortSubMenu(wrap_item_new,wrap_item_original,col_number,plus+1);
        else $(wrap_item_original).remove();
    }
</script>
<script>
    $("#main-menu .item").each(function(){
        if(!$(this).hasClass("formated")){
            var idBoxNew = $(this).find(".box_new").attr("id");
            var idBoxOriginal = $(this).find(".box_original").attr("id");
            autoSortSubMenu("#"+idBoxNew,"#"+idBoxOriginal,5,0);
        }
        $(this).addClass("formated");
    });
</script>


<!-- popup template global-->

<div id="adv-popup">

    <div class="banner">
        <a class="close" href="javascript:void(0)" onclick="closePop();"><i class="fa fa-times-circle"></i></a>
        <a href="/hura-test-landing-page.html" target="_blank"><img src="/media/banner/popup_POP-UP-01.png" alt="banner popup"/></a>
    </div>
    <div class="background"></div>

</div>
<script>
    if($.session.get('session_popup')!='true') {
        $.session.set('session_popup','true');
    }else{
        $("#adv-popup").hide();
    }
</script>

<!--Popup-->
<script>
    $(window).scroll(function(){
        if($(window).scrollTop() > 200) {
            $(".banner-fix-right").addClass("banner-fix-scroll");
            $(".banner-fix-left").addClass("banner-fix-scroll");
        }else {
            $(".banner-fix-right").removeClass("banner-fix-scroll");
            $(".banner-fix-left").removeClass("banner-fix-scroll");
        }
    });
</script>





<script>
    $(".banner-products-list").owlCarousel({
        items:2,
        dots:false,
        margin:10,
        autoplay:true,
        autoplayTimeout:3000,
        autoplaySpeed:800,
        loop:true,
        nav:true,
        navText:['<i class="fa fa-chevron-circle-left"></i>','<i class="fa fa-chevron-circle-right"></i>']
    });
</script>






















<!-- Your customer chat code -->
<div class="fb-customerchat"
     attribution=setup_tool
     page_id="1379621225591939">
</div>

<div id="fb-root"></div>
<div id="facebook-lib"></div>
<div id="addthis-lib"></div>
<script src="https://uhchat.net/code.php?f=235088"></script>
<!-- Load Facebook SDK for JavaScript -->
<script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml            : true,
            version          : 'v4.0'
        });
    };
</script>
<script>
    setTimeout(function(){
        $("#addthis-lib").html('\x3Cscript type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5148003b01a03b86">\x3C/script>');
        //$("#facebook-lib").html('\x3Cscript async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.3&appId=&autoLogAppEvents=1">\x3C/script>');


        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    },3000);
</script>
