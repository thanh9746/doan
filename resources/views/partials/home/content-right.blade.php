<div id="content_right">
    <div class="box_right">
        <div class="title_box_right">
            <h2>Máy tính xách tay</h2>
        </div>
        <div class="content_box">
            <ul class="ul">


                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="/may-tinh-xach-tay/laptop-dell.html">Laptop Dell</a> </li>

                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="/may-tinh-xach-tay/laptop-lenovo-thinkpad.html">Laptop lenovo thinkpad</a> </li>

                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="/may-tinh-xach-tay/laptop-hp.html">Laptop HP</a> </li>

                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="/may-tinh-xach-tay/laptop-asus.html">Laptop Asus</a> </li>

                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="/laptop-lenovo.html">Laptop Lenovo</a> </li>

                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="/may-tinh-xach-tay/laptop-acer.html">Laptop Acer</a> </li>

                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="/may-tinh-xach-tay/laptop-msi.html">Laptop MSI</a> </li>

                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="/may-tinh-xach-tay/laptop-lg-gram.html">Laptop LG Gram</a> </li>

                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="/laptop-fujitsu.html">Laptop Fujitsu</a> </li>


            </ul>
        </div>
    </div><!--box_right-->

    <div class="box_right filter brand">
        <div class="title_box_right black">
            <div class="title-filter-collapse">Hãng sản xuất <span class="icon">+</span></div>
        </div>
        <div class="content_box" style="display:none;">
            <ul class="ul">

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?brand=1'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?brand=1">Laptop Acer</a> (31)</li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?brand=5'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?brand=5">Laptop Asus</a> (161)</li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?brand=2'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?brand=2">Laptop Dell</a> (85)</li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?brand=4'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?brand=4">Laptop HP</a> (85)</li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?brand=3'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?brand=3">Laptop Lenovo</a> (62)</li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?brand=7'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?brand=7">Laptop LG Gram</a> (11)</li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?brand=6'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?brand=6">Laptop MSI</a> (10)</li>

            </ul>
        </div>
    </div><!--box_right-->


    <div class="box_right filter">
        <div class="title_box_right black">
            <div class="title-filter-collapse">Khoảng giá <span class="icon">+</span></div>
        </div>
        <div class="content_box" style="display:none;">
            <ul class="ul">

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?max=8000000'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?max=8000000">Dưới 8 triệu</a> <span>(22)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?max=10000000&amp;min=8000000'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?max=10000000&amp;min=8000000">8 triệu - 10 triệu</a> <span>(17)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?max=12000000&amp;min=10000000'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?max=12000000&amp;min=10000000">10 triệu - 12 triệu</a> <span>(51)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?max=15000000&amp;min=12000000'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?max=15000000&amp;min=12000000">12 triệu - 15 triệu</a> <span>(102)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?max=20000000&amp;min=15000000'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?max=20000000&amp;min=15000000">15 triệu - 20 triệu</a> <span>(124)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?max=25000000&amp;min=20000000'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?max=25000000&amp;min=20000000">20 triệu - 25 triệu</a> <span>(69)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?max=30000000&amp;min=25000000'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?max=30000000&amp;min=25000000">25 triệu - 30 triệu</a> <span>(35)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?max=35000000&amp;min=30000000'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?max=35000000&amp;min=30000000">30 triệu - 35 triệu</a> <span>(21)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?min=35000000'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?min=35000000">Trên 35 triệu</a> <span>(36)</span></li>

            </ul>
        </div>
    </div><!--box_right-->


    <div class="box_right filter">
        <div class="title_box_right black">
            <div class="title-filter-collapse">Bộ vi xử lý <span class="icon">+</span></div>
        </div>
        <div class="content_box" style="display:none;">
            <ul class="ul">

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C29%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C29%2C">Pentium, Celeron</a> <span>(14)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C21%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C21%2C">Core i3</a> <span>(68)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C22%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C22%2C">Core i5</a> <span>(230)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C23%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C23%2C">Core i7</a> <span>(113)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C24%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C24%2C">Core i9</a> <span>(3)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C25%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C25%2C">Ryzen 3</a> <span>(13)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C26%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C26%2C">Ryzen 5</a> <span>(21)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C27%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C27%2C">Ryzen 7</a> <span>(4)</span></li>

            </ul>
        </div>
    </div><!--box_right-->

    <div class="box_right filter">
        <div class="title_box_right black">
            <div class="title-filter-collapse">Ram <span class="icon">+</span></div>
        </div>
        <div class="content_box" style="display:none;">
            <ul class="ul">

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C31%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C31%2C">4GB</a> <span>(175)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C32%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C32%2C">8GB</a> <span>(251)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C33%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C33%2C">16GB</a> <span>(32)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C34%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C34%2C">32GB</a> <span>(6)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C120%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C120%2C">64GB</a> <span>(1)</span></li>

            </ul>
        </div>
    </div><!--box_right-->

    <div class="box_right filter">
        <div class="title_box_right black">
            <div class="title-filter-collapse">Dung lượng HDD <span class="icon">+</span></div>
        </div>
        <div class="content_box" style="display:none;">
            <ul class="ul">

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C39%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C39%2C">32GB eMMC</a> <span>(3)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C36%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C36%2C">500GB</a> <span>(1)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C37%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C37%2C">1TB</a> <span>(88)</span></li>

            </ul>
        </div>
    </div><!--box_right-->

    <div class="box_right filter">
        <div class="title_box_right black">
            <div class="title-filter-collapse">Dung lượng SSD <span class="icon">+</span></div>
        </div>
        <div class="content_box" style="display:none;">
            <ul class="ul">

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C42%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C42%2C">128GB</a> <span>(8)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C43%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C43%2C">256GB</a> <span>(144)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C44%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C44%2C">512GB</a> <span>(219)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C45%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C45%2C">1TB</a> <span>(10)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C121%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C121%2C">1.5TB</a> <span>(1)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C119%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C119%2C">2TB</a> <span>(1)</span></li>

            </ul>
        </div>
    </div><!--box_right-->

    <div class="box_right filter">
        <div class="title_box_right black">
            <div class="title-filter-collapse">Card đồ họa <span class="icon">+</span></div>
        </div>
        <div class="content_box" style="display:none;">
            <ul class="ul">

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C47%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C47%2C">VGA rời</a> <span>(144)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C48%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C48%2C">VGA onboard</a> <span>(292)</span></li>

            </ul>
        </div>
    </div><!--box_right-->

    <div class="box_right filter">
        <div class="title_box_right black">
            <div class="title-filter-collapse">Kích thước màn hình <span class="icon">+</span></div>
        </div>
        <div class="content_box" style="display:none;">
            <ul class="ul">

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C65%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C65%2C">12.5inch</a> <span>(3)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C66%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C66%2C">13.3inch / 13.5inch</a> <span>(54)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C67%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C67%2C">14.0inch</a> <span>(225)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C68%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C68%2C">15.6inch</a> <span>(172)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C69%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C69%2C">17.3inch</a> <span>(2)</span></li>

            </ul>
        </div>
    </div><!--box_right-->

    <div class="box_right filter">
        <div class="title_box_right black">
            <div class="title-filter-collapse">Màn hình cảm ứng <span class="icon">+</span></div>
        </div>
        <div class="content_box" style="display:none;">
            <ul class="ul">

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C63%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C63%2C">Không cảm ứng</a> <span>(377)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C62%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C62%2C">Có cảm ứng</a> <span>(62)</span></li>

            </ul>
        </div>
    </div><!--box_right-->

    <div class="box_right filter">
        <div class="title_box_right black">
            <div class="title-filter-collapse">Màu sắc <span class="icon">+</span></div>
        </div>
        <div class="content_box" style="display:none;">
            <ul class="ul">

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C124%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C124%2C">Green</a> <span>(1)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C61%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C61%2C">Red</a> <span>(3)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C60%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C60%2C">Silver</a> <span>(202)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C59%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C59%2C">Gray</a> <span>(77)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C58%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C58%2C">Rose Gold</a> <span>(3)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C57%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C57%2C">Gold</a> <span>(22)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C56%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C56%2C">Pink</a> <span>(6)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C70%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C70%2C">Blue</a> <span>(28)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C55%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C55%2C">Black</a> <span>(104)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C122%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C122%2C">White</a> <span>(9)</span></li>

            </ul>
        </div>
    </div><!--box_right-->

    <div class="box_right filter">
        <div class="title_box_right black">
            <div class="title-filter-collapse">Trọng lượng <span class="icon">+</span></div>
        </div>
        <div class="content_box" style="display:none;">
            <ul class="ul">

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C50%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C50%2C">&lt; 1.0 kg</a> <span>(13)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C51%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C51%2C">1.0kg - 1.2kg</a> <span>(39)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C52%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C52%2C">1.2kg - 1.5kg</a> <span>(136)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C53%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C53%2C">1.5kg - 2kg</a> <span>(246)</span></li>

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C54%2C'"> <a href="https://laptopworld.vn/may-tinh-xach-tay.html?filter=%2C54%2C">&gt; 2kg</a> <span>(33)</span></li>

            </ul>
        </div>
    </div><!--box_right-->



</div>
