<header id="header">
    <div class="container">
        <a href="/" class="logo" style="margin-top:15px"><img src="/template/default/images/logo.png" alt="Laptopworld" style="height:60px;"> </a>
        <div class="main-search">
            <form name="search" action="/tim">
                <div class="form-search">
                    <input type="text" name="q" id="text-search" placeholder="Nhập từ khóa tìm kiếm" autocomplete="off">
                    <button type="submit" class="search-btn fa fa-search"></button>
                </div>
            </form>
            <div class="autocomplete-suggestions" style="display: none;"></div>
        </div>
        <div id="header-right">
            <a class="item hotline" href="tel:0961560888">
                <i class="icon fa fa-volume-control-phone"></i>
                <span>Mua hàng online</span><b>0961560888</b>
            </a>
            <a href="/buildpc" class="item buildpc">
                <i class="icon icon-buildpc"></i>
                <b>Xây dựng cấu hình</b>
            </a>
        </div><!--header-right-->
    </div><!--container-->
</header>
