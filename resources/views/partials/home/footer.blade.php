<div class="container mt-3">

    <div id="top-footer">
        <div class="fanpage">
            <div class="fb-page fb_iframe_widget" data-href="https://www.facebook.com/hoangminhtechco/" data-tabs="timeline" data-width="450" data-height="269" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" fb-xfbml-state="rendered" fb-iframe-plugin-query="adapt_container_width=true&amp;app_id=&amp;container_width=450&amp;height=269&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fhoangminhtechco%2F&amp;locale=en_US&amp;sdk=joey&amp;show_facepile=true&amp;small_header=true&amp;tabs=timeline&amp;width=450"><span style="vertical-align: bottom; width: 450px; height: 269px;"><iframe name="f149bb3b71970a" width="450px" height="269px" data-testid="fb:page Facebook Social Plugin" title="fb:page Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v4.0/plugins/page.php?adapt_container_width=true&amp;app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D46%23cb%3Df654775f534568%26domain%3Dlaptopworld.vn%26origin%3Dhttps%253A%252F%252Flaptopworld.vn%252Ff2954d351a10b%26relation%3Dparent.parent&amp;container_width=450&amp;height=269&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fhoangminhtechco%2F&amp;locale=en_US&amp;sdk=joey&amp;show_facepile=true&amp;small_header=true&amp;tabs=timeline&amp;width=450" style="border: none; visibility: visible; width: 450px; height: 269px;" class=""></iframe></span></div>
        </div><!--fanpage-->
        <div class="box-right">
            <div class="item">
                <div class="title">YOUTUBE <a href=""><i class="icon icon-regist-chanel-youtube"></i></a></div>
                <div class="content">
                    <div class="videoWrapper js-load-video loaded"><iframe width="560" height="315" src="https://www.youtube.com/embed/Iyc99HJvMo8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe></div>
                </div><!--content-->
            </div><!--item-->
            <div class="item">
                <div class="title">Tổng đài trợ giúp</div>
                <div class="content">
                    <b>Hỗ trợ Online</b>1900 6838 - (024) 7305 6888<br>
                    <b>Nhân viên bán hàng Online – Trả góp: </b> (024) 7305 6888 Máy lẻ 101, 102 <br>
                    <b>Nhân viên Bán lẻ : </b>(024) 7305 6888 Máy lẻ 101, 102 ; 034 201 4444 - 096156 0888<br>
                    <b>Nhân viên bán hàng dự án: </b> 0965588699<br>
                    <b>Nhân viên kỹ thuật: </b> (024) 7305 6888 Máy lẻ 104 <br>
                    <b>Nhân viên kế toán: </b> (024) 7305 6888 Máy lẻ 107<br>
                    <b>Email: </b> hoangminh@laptopworld.com.vn<br>
                </div><!--content-->
            </div><!--item-->
        </div><!--box-right-->
    </div><!--top-footer-->
</div>
<footer>
    <div class="container">
        <div class="col-info">
            <div class="title">Thông tin chung</div>
            <div class="content">
                <a href="/gioi-thieu" target="_blank">Giới thiệu công ty</a>
                <a href="/tuyen-dung.html" target="_blank">Tuyển dụng</a>
                <a href="/tin-tuc" target="_blank">Tin tức</a>
                <a href="/lien-he" rel="nofollow">Ý kiến khách hàng</a>
                <a href="/lien-he" target="_blank">Liên hệ - hợp tác</a>
                <a href="/thong-tin-cong-ty/thong-tin-tai-khoan.html" target="_blank">Thông tin tài khoản</a>
            </div>
        </div><!--col-info-->
        <div class="col-info">
            <div class="title">Chính sách chung</div>
            <div class="content">
                <a href="/chinh-sach-uu-dai/chinh-sach-van-chuyen-giao-nhan.html">Chính sách vận chuyển</a>
                <a href="/chinh-sach-uu-dai/chinh-sach-bao-hanh.html">Chính sách bảo hành</a>
                <a href="/huong-dan-khach-hang/chinh-sach-doitra-hang-hoan-tien.html">Chính sách đổi, trả lại hàng</a>
                <a href="/huong-dan-khach-hang/chinh-sach-doitra-hang-hoan-tien.html">Chính sách hoàn tiền</a>
                <a href="/phuong-thuc-thanh-toan.html">Phương thức thanh toán</a>
                <a href="/chinh-sach-hang-chinh-hang.html">Chính sách hàng chính hãng</a>
                <a href="/chinh-sach-uu-dai/chinh-sach-bao-mat-thong-tin.html">Bảo mật thông tin khách hàng</a>
            </div>
        </div><!--col-info-->
        <div class="col-info">
            <div class="title">Thông tin khuyến mãi</div>
            <div class="content">
                <a href="/san-pham-xa-hang">Sản phẩm khuyến mại</a>
                <a href="/san-pham-ban-chay">Sản phẩm bán chạy</a>
                <a href="/san-pham-moi">Sản phẩm mới</a>
                <a href="http://online.gov.vn/CompanyDisplay.aspx?DocId=6504" target="_blank"><i class="icon icon-bct"></i></a>
            </div>
        </div><!--col-info-->
        <div class="col-info">
            <div class="title">Hỗ trợ khách hàng</div>
            <div class="content">
                <a href="/huong-dan-khach-hang/huong-dan-mua-hang-online.html">Mua hàng trực tuyến</a>
                <a href="/thu-tuc-tra-gop/huong-dan-mua-hang-tra-gop.html">Hướng dẫn mua trả góp</a>
                <a href="/huong-dan-khach-hang/huong-dan-mua-hang-online.html">Hướng dẫn mua hàng Online</a>
                <a href="/tao-link-tra-gop-0-online.html">Tạo Link trả góp 0% Online</a>
                <a href="/thu-tuc-tra-gop/thu-tuc-tra-gop-hdsaison.html">Trả góp HD SaiSon</a>
                <a href="/thu-tuc-tra-gop/thu-tuc-tra-gop-acs.html">Thủ tục trả góp ACS</a>
                <a href="/thong-tin-cong-ty/tong-dai-ho-tro.html">Tổng đài hỗ trợ trực tuyến</a>
            </div>
        </div><!--col-info-->
        <div class="col-info">
            <div class="title">Thanh toán an toàn</div>
            <div class="content">

                <a href="" class="hide-mobile"><i class="icon icon-payment"></i></a>
                <a href="" class="hide-pc"><img class="lazy" src="/template/default/images/blank.png" data-src="/template/default/images/mobile_img_payment_accept.png" alt="thanh toán"></a><!--hiện ở mobile-->

                <div class="newsletter d-none d-lg-block">
                    <div class="title">Nhận tin khuyến mãi</div>
                    <input type="text" id="email-newsletter" class="form-control" placeholder="Nhập email nhận tin KM">
                    <button type="button" class="btn btn-primary" onclick="subscribe_newsletter('#email-newsletter')">Gửi</button>
                </div>
            </div>
        </div><!--col-info-->
        <div class="clearfix"></div>
        <div id="copyright">
            Copyright 2019 © - Bản quyền thuộc về công ty TNHH Công Nghệ Tin Học Hoàng Minh<br>
            Địa chỉ: Số 10 Ngõ 117 Thái Hà, Đống Đa, Hà Nội
            - GPKD số:0104078681 do Sở Kế Hoạch và Đầu Tư Thành Phố Hà Nội cấp
        </div><!--copyright-->
    </div><!--container-->
</footer>
