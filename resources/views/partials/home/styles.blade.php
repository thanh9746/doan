<link rel="stylesheet" href="{{asset ('home/css/hura.css')}}">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:700|Roboto:400,500,700&display=swap&subset=vietnamese" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:600&display=swap" rel="stylesheet">
<link rel="stylesheet" href="{{asset ('home/css/home.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />

