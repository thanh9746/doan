<div id="right_pro_page">

    <div class="box_right filter">
        <div class="title_box_right black">
            <h2 class="title-filter-collapse active">Hãng sản xuất <span class="icon">-</span></h2>
        </div>
        <div class="content_box">
            <div class="bg_gradient_bottom_title"></div>
            <ul class="ul">

                <li><input class="" type="checkbox" onclick="location.href='https://laptopworld.vn/lg-gram-2020.html?brand=7'"> <a href="https://laptopworld.vn/lg-gram-2020.html?brand=7">Laptop LG Gram</a> <span>(6)</span></li>

            </ul>
        </div>
    </div><!--box_right-->




    <div class="clearfix mt-2"></div>
    <div class="box_right_detail">
        <div class="title_box_right_detail"><span>Có thể bạn quan tâm</span></div>
        <div class="content_box_right_detail product_list">
            <div class="product-list product-list-item-full loaded" id="similar_category_product" data-url="/ajax/get_json.php?action=product&amp;action_type=product-list&amp;type=&amp;category=551&amp;show=6&amp;sort=order" data-holder="#similar_category_product" data-limit="6"><div class="p-item"><div class="p-container"><a href="/lg-gram-2020-14zd90n-v.ax53a5.html" class="p-img"><img src="/media/product/5755_14zd90n_v_ax53a5.jpg" alt="LG Gram 2020 14ZD90N-V.AX53A5"></a><span class="p-price"><span class="price-border">28.990.000</span><span class="price-shadow">28.990.000</span></span><span class="p-old-price"></span><a href="/lg-gram-2020-14zd90n-v.ax53a5.html" class="p-name">LG Gram 2020 14ZD90N-V.AX53A5</a><div class="p-bottom"><span class="stock instock"><i class="fa fa-check"></i> Có hàng</span><span class="p-buy" onclick="listenBuyProduct(5755,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span></div></div><div class="hover_content_pro"><a href="/lg-gram-2020-14zd90n-v.ax53a5.html" class="hover_name">LG Gram 2020 14ZD90N-V.AX53A5</a><table><tbody><tr><td colspan="2"><span class="p-price-full"><span class="price-border">28.990.000</span><span class="price-shadow">28.990.000</span></span><span class="hover_vat"></span></td></tr><tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng chính hãng LG Việt Nam, bảo hành tại nhà</td></tr><tr><td><b>Kho hàng:</b></td><td><b>Còn hàng</b></td></tr></tbody></table><div class="hori_line"></div><div class="hover_summary"><b>Mô tả tóm tắt:</b><br><span style="white-space:pre-line;">CPU: Intel Core i5-1035G7 (1.2GHz up to 3.7GHz 6MB Cache)
Ram: 8GB DDR4 2400MHz Onboard (Còn 1 khe ram, up to 24GB SDRAM)
Ổ cứng: 256GB PCIe® NVMe™ M.2 SSD (còn trống 1 khe SSD)
Display: 14" FHD (1920 x 1080) IPS, sRGB 96%, 300nits
VGA: Intel® Iris® Plus Graphics
Pin: 4 Cell 72WHr (9,450 mAh)
Color: White
Weight: 999 g
OS: Free DOS</span></div></div></div><div class="p-item"><div class="p-container"><a href="/lg-gram-2020-14zd90n-v.ax55a5.html" class="p-img"><img src="/media/product/5757_v_ax55a5.jpg" alt="LG Gram 2020 14ZD90N-V.AX55A5"></a><span class="p-price"><span class="price-border">30.490.000</span><span class="price-shadow">30.490.000</span></span><span class="p-old-price"></span><a href="/lg-gram-2020-14zd90n-v.ax55a5.html" class="p-name">LG Gram 2020 14ZD90N-V.AX55A5</a><div class="p-bottom"><span class="stock instock"><i class="fa fa-check"></i> Có hàng</span><span class="p-buy" onclick="listenBuyProduct(5757,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span></div></div><div class="hover_content_pro"><a href="/lg-gram-2020-14zd90n-v.ax55a5.html" class="hover_name">LG Gram 2020 14ZD90N-V.AX55A5</a><table><tbody><tr><td colspan="2"><span class="p-price-full"><span class="price-border">30.490.000</span><span class="price-shadow">30.490.000</span></span><span class="hover_vat"></span></td></tr><tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng chính hãng LG Việt Nam, bảo hành tại nhà</td></tr><tr><td><b>Kho hàng:</b></td><td><b>Còn hàng</b></td></tr></tbody></table><div class="hori_line"></div><div class="hover_summary"><b>Mô tả tóm tắt:</b><br><span style="white-space:pre-line;">CPU: Intel Core i5-1035G7 (1.2GHz up to 3.7GHz 6MB Cache)
Ram: 8GB DDR4 2400MHz Onboard (Còn 1 khe ram, up to 24GB SDRAM)
Ổ cứng: 512GB PCIe® NVMe™ M.2 SSD (còn trống 1 khe SSD)
Display: 14" FHD (1920 x 1080) IPS, sRGB 96%, 300nits
VGA: Intel® Iris® Plus Graphics
Pin: 4 Cell 72WHr (9,450 mAh)
Color: Dark Silver
Weight: 999 g
OS: Free DOS</span></div></div></div><div class="p-item"><div class="p-container"><a href="/lg-gram-2020-15zd90n-v.ax56a5.html" class="p-img"><img src="/media/product/5759_15zd90n_v_ax56a5.jpg" alt="LG Gram 2020 15ZD90N-V.AX56A5"></a><span class="p-price"><span class="price-border">31.990.000</span><span class="price-shadow">31.990.000</span></span><span class="p-old-price"></span><a href="/lg-gram-2020-15zd90n-v.ax56a5.html" class="p-name">LG Gram 2020 15ZD90N-V.AX56A5</a><div class="p-bottom"><span class="stock instock"><i class="fa fa-check"></i> Có hàng</span><span class="p-buy" onclick="listenBuyProduct(5759,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span></div></div><div class="hover_content_pro"><a href="/lg-gram-2020-15zd90n-v.ax56a5.html" class="hover_name">LG Gram 2020 15ZD90N-V.AX56A5</a><table><tbody><tr><td colspan="2"><span class="p-price-full"><span class="price-border">31.990.000</span><span class="price-shadow">31.990.000</span></span><span class="hover_vat"></span></td></tr><tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng chính hãng LG Việt Nam, bảo hành tại nhà</td></tr><tr><td><b>Kho hàng:</b></td><td><b>Còn hàng</b></td></tr></tbody></table><div class="hori_line"></div><div class="hover_summary"><b>Mô tả tóm tắt:</b><br><span style="white-space:pre-line;">CPU: Intel Core i5-1035G7 (1.2GHz up to 3.7GHz 6MB Cache)
Ram: 8GB DDR4 2400MHz Onboard (Còn 1 khe ram, up to 24GB SDRAM)
Ổ cứng: 512GB PCIe® NVMe™ M.2 SSD (còn trống 1 khe SSD)
Display: 15.6" FHD (1920 x 1080) IPS, sRGB 96%, 300nits
VGA: Intel® Iris® Plus Graphics
Pin: 2Cell 80WHr (10,336 mAh)
Color: White
Weight: 1120g
OS: Free DOS</span></div></div></div><div class="p-item"><div class="p-container"><a href="/lg-gram-2020-15z90n-v.ar55a5.html" class="p-img"><img src="/media/product/5760_15z90n_v_ar55a5.jpg" alt="LG Gram 2020 15Z90N-V.AR55A5 "></a><span class="p-price"><span class="price-border">36.990.000</span><span class="price-shadow">36.990.000</span></span><span class="p-old-price"></span><a href="/lg-gram-2020-15z90n-v.ar55a5.html" class="p-name">LG Gram 2020 15Z90N-V.AR55A5 </a><div class="p-bottom"><span class="stock instock"><i class="fa fa-check"></i> Có hàng</span><span class="p-buy" onclick="listenBuyProduct(5760,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span></div></div><div class="hover_content_pro"><a href="/lg-gram-2020-15z90n-v.ar55a5.html" class="hover_name">LG Gram 2020 15Z90N-V.AR55A5 </a><table><tbody><tr><td colspan="2"><span class="p-price-full"><span class="price-border">36.990.000</span><span class="price-shadow">36.990.000</span></span><span class="hover_vat"></span></td></tr><tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng chính hãng LG Việt Nam, bảo hành tại nhà</td></tr><tr><td><b>Kho hàng:</b></td><td><b>Còn hàng</b></td></tr></tbody></table><div class="hori_line"></div><div class="hover_summary"><b>Mô tả tóm tắt:</b><br><span style="white-space:pre-line;">CPU: Intel Core i5-1035G7 (1.2GHz up to 3.7GHz 6MB Cache)
Ram: 8GB DDR4 2400MHz Onboard (Còn 1 khe ram, up to 24GB SDRAM)
Ổ cứng: 512GB PCIe® NVMe™ M.2 SSD (còn trống 1 khe SSD)
Display: 15.6" FHD (1920 x 1080) IPS, sRGB 96%, 300nits
VGA: Intel® Iris® Plus Graphics
Pin: 4 Cell 80WHr  (10.336mAh)
Color: Dark Silver
Weight: 1120g
OS: Windows 10 Home bản quyền</span></div></div></div><div class="p-item"><div class="p-container"><a href="/lg-gram-2020-14z90n-v.ar52a5.html" class="p-img"><img src="/media/product/5758_14z90n_v_ar52a5.jpg" alt="LG Gram 2020 14Z90N-V.AR52A5"></a><span class="p-price"><span class="price-border">31.990.000</span><span class="price-shadow">31.990.000</span></span><span class="p-old-price"></span><a href="/lg-gram-2020-14z90n-v.ar52a5.html" class="p-name">LG Gram 2020 14Z90N-V.AR52A5</a><div class="p-bottom"><span class="stock instock"><i class="fa fa-check"></i> Có hàng</span><span class="p-buy" onclick="listenBuyProduct(5758,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span></div></div><div class="hover_content_pro"><a href="/lg-gram-2020-14z90n-v.ar52a5.html" class="hover_name">LG Gram 2020 14Z90N-V.AR52A5</a><table><tbody><tr><td colspan="2"><span class="p-price-full"><span class="price-border">31.990.000</span><span class="price-shadow">31.990.000</span></span><span class="hover_vat"></span></td></tr><tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng chính hãng LG Việt Nam, bảo hành tại nhà</td></tr><tr><td><b>Kho hàng:</b></td><td><b>Còn hàng</b></td></tr></tbody></table><div class="hori_line"></div><div class="hover_summary"><b>Mô tả tóm tắt:</b><br><span style="white-space:pre-line;">CPU: Intel Core i5-1035G7 (1.2GHz up to 3.7GHz 6MB Cache)
Ram: 8GB DDR4 2400MHz Onboard (Còn 1 khe ram, up to 24GB SDRAM)
Ổ cứng: 256GB PCIe® NVMe™ M.2 SSD (còn trống 1 khe SSD)
Display: 14" FHD (1920 x 1080) IPS, sRGB 96%, 300nits
VGA: Intel® Iris® Plus Graphics
Pin: 4 Cell 72WHr (9.450mAh)
Color: Dark Silver
Weight: 999 g
OS: Windows 10 Home bản quyền</span></div></div></div><div class="p-item"><div class="p-container"><a href="/lg-gram-2020-17z90n-v.ah75a5.html" class="p-img"><img src="/media/product/5761_17z90n_v_ah75a5.jpg" alt="LG Gram 2020 17Z90N-V.AH75A5"></a><span class="p-price"><span class="price-border">42.490.000</span><span class="price-shadow">42.490.000</span></span><span class="p-old-price"></span><a href="/lg-gram-2020-17z90n-v.ah75a5.html" class="p-name">LG Gram 2020 17Z90N-V.AH75A5</a><div class="p-bottom"><span class="stock instock"><i class="fa fa-check"></i> Có hàng</span><span class="p-buy" onclick="listenBuyProduct(5761,0,1)"><i class="fa fa-shopping-cart"></i> Giỏ hàng</span></div></div><div class="hover_content_pro"><a href="/lg-gram-2020-17z90n-v.ah75a5.html" class="hover_name">LG Gram 2020 17Z90N-V.AH75A5</a><table><tbody><tr><td colspan="2"><span class="p-price-full"><span class="price-border">42.490.000</span><span class="price-shadow">42.490.000</span></span><span class="hover_vat"></span></td></tr><tr><td width="90"><b>Bảo hành:</b></td><td>12 Tháng chính hãng LG Việt Nam, bảo hành tại nhà</td></tr><tr><td><b>Kho hàng:</b></td><td><b>Còn hàng</b></td></tr></tbody></table><div class="hori_line"></div><div class="hover_summary"><b>Mô tả tóm tắt:</b><br><span style="white-space:pre-line;">CPU: Intel Core i7-1065G7 (1.30GHz up to 3.90GHz, 8MB Cache)
Ram: 8GB DDR4 2400MHz Onboard (Còn 1 khe ram, up to 24GB SDRAM)
Ổ cứng: 512GB PCIe® NVMe™ M.2 SSD (còn trống 1 khe SSD)
Display: 17" WQXGA (2560 x 1600) IPS
VGA: Intel® Iris® Plus Graphics
Pin: 4 Cell 80WHr  (10.336mAh)
Color: Dark Silver
Weight: 1350g
OS: Windows 10 Home bản quyền</span></div></div></div></div>
        </div><!--content_box_right_detail-->
    </div><!--box_right_detail-->

</div>
