<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title', '| Home')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('partials.home.styles')
    @yield('styles')
</head>
<body >
<!-- Header -->
@include('partials.home.header')

    <!-- Navbar -->
@include('partials.home.nav')

<div class="clearfix"></div>

<div class="container">
        @yield('container')
</div>


<!-- Main Banner -->

<div class="clearfix"></div>

@include('partials.home.footer')

<!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
@include('partials.home.scripts')
</body>
</html>
